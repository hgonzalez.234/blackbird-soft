#include <opencv2/opencv.hpp>
#include <memory>


class BBCamera
{

public:

	/**
	 * @brief      { stereo camera driver }
	 *
	 * @param[in]  resolution  The resolution
	 * @param[in]  frame_rate  The frame rate
	 */
  BBCamera(double frame_rate);

  ~BBCamera();

  bool isOpened() const;

	/**
	 * @brief      Sets the resolution.
	 *
	 * @param[in]  type  The type
	 */
  void setResolution(int type);

	/**
	 * @brief      Sets the frame rate.
	 *
	 * @param[in]  frame_rate  The frame rate
	 */
  void setFrameRate(double frame_rate);

	/**
	 * @brief      Gets the images.
	 *
	 * @param      left_image   The left image
	 * @param      right_image  The right image
	 *
	 * @return     The images.
	 */
  bool getImages(cv::Mat& left_image, cv::Mat& right_image);

  bool getImages2(cv::Mat& left_image, cv::Mat& right_image);


  double getFrameRate();


private:
	std::unique_ptr<cv::VideoCapture> camera_;
	int width_;
	int height_;
	double frame_rate_;
};
