#include <stdio.h>
#include <string.h>

#include <opencv2/opencv.hpp>
#include "BlackbirdCamera.hpp"

int main() {

  double frame_rate = 15.0;
  BBCamera bb_camera(frame_rate);

  cv::Mat left_frame, right_frame;

  while (true) {
    bb_camera.getImages(left_frame, right_frame);
    cv::imshow("left frame", left_frame);
    cv::imshow("right frame", right_frame);
    cv::waitKey(10);
  }

  return 0;
}

