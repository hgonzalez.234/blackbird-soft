#include "BlackbirdCamera.hpp"

/**
 * @brief      { stereo camera driver }
 *
 * @param[in]  resolution  The resolution
 * @param[in]  frame_rate  The frame rate
 */
BBCamera::BBCamera(double frame_rate) {
  int device_id;
  std::cout << "Ingrese device ID" << std::endl;
  std::cin >> device_id;
  camera_ = std::unique_ptr<cv::VideoCapture>( new cv::VideoCapture(device_id) );

  // set frame rate
  // this function doesn't work very well in current Opencv 2.4, so, just use ROS to control frame rate.
  setFrameRate( frame_rate );

  width_ = camera_->get(CV_CAP_PROP_FRAME_WIDTH);
  height_ = camera_->get(CV_CAP_PROP_FRAME_HEIGHT);

  std::cout << "Stereo Camera Resolution: " << width_ << "x" << height_ << std::endl;
  std::cout << "Stereo Camera Set Frame Rate: " << camera_->get(CV_CAP_PROP_FPS) << std::endl;
}

BBCamera::~BBCamera() {}


bool BBCamera::isOpened() const {
  return camera_->isOpened();
}

/**
 * @brief      Sets the resolution.
 *
 * @param[in]  type  The type
 */
void BBCamera::setFrameRate(double frame_rate) {
  camera_->set(CV_CAP_PROP_FPS, frame_rate);
  frame_rate_ = camera_->get(CV_CAP_PROP_FPS);
}

/**
 * @brief      Gets the images.
 *
 * @param      left_image   The left image
 * @param      right_image  The right image
 *
 * @return     The images.
 */
bool BBCamera::getImages(cv::Mat& left_image, cv::Mat& right_image) {
  cv::Mat raw;
  if (camera_->grab()) {
    camera_->retrieve(raw);
    cv::Rect left_rect(0, 5, (width_ / 2) - 10, (height_ - 5));
    cv::Rect right_rect(width_ / 2, 5, (width_ / 2) - 10, (height_ - 5));
    left_image = raw(left_rect);
    right_image = raw(right_rect);
    return true;
  } else {
    return false;
  }
}

bool BBCamera::getImages2(cv::Mat& left_image, cv::Mat& right_image) {
  cv::Mat frame;
  // Get a new frame from camera
  *camera_ >> frame;
  // Extract left and right images from side-by-side
  left_image = frame(cv::Rect(0, 5, (frame.cols / 2) - 10, frame.rows));
  right_image = frame(cv::Rect(frame.cols / 2, 5, (frame.cols / 2) - 10, frame.rows));
  // Display images
//  cv::imshow("frame", frame);
//  cv::imshow("left", left_image);
//  cv::imshow("right", right_image);
//  cv::waitKey(30);
}

double BBCamera::getFrameRate() {
  return frame_rate_;
}
