#include <iostream>
#include <sstream>
#include <csignal>
#include <unistd.h>
#include <atomic>         // std::atomic, std::atomic_flag, ATOMIC_FLAG_INIT
#include <thread>
#include <opencv2/opencv.hpp>
#include <errno.h>
#include <sys/stat.h>

#include "ConcurrentQueue.hpp"
#include "BlackbirdCamera.hpp"
#include "./utils/Logger.hpp"
#include "./utils/timer.h"


std::atomic<int> stop_main (0);
std::atomic<int> stop_consumer (0);
std::atomic<int> stop_producer (0);
std::atomic<int> stop_viewer (0);


void signalHandler(int signum)
{
    stop_consumer.store(1);
    stop_main.store(1);
	stop_viewer.store(1);
}

// bb parameters
double frame_rate = 15.0; // 15-60 hz

// create camera bb object
BBCamera bb_camera(frame_rate);

struct data {
  double timestamp;
  cv::Mat left_frame;
  cv::Mat right_frame;
};

cv::Mat left_image, right_image;

void produce(Queue<data>& q) {
  cv::Mat left_frame, right_frame;
  while (!stop_producer.load()) {
    bb_camera.getImages(left_frame, right_frame);
    data stereoFrame = {
      Timer::now(),
      left_frame,
      right_frame,
    };

    q.push(stereoFrame);

  }
}

void consume(Queue<data>& q) {
  static double last = 0, actual;
  while ( !stop_consumer.load() ) {
    auto stereoFrame = q.pop();
    cv::imwrite("left_images/left_"+std::to_string(stereoFrame.timestamp)+".png", stereoFrame.left_frame);
    cv::imwrite("right_images/right_"+std::to_string(stereoFrame.timestamp)+".png", stereoFrame.right_frame);
	actual = stereoFrame.timestamp;
	if (((actual - last) > 0.1)) {
		left_image = stereoFrame.left_frame;
		right_image = stereoFrame.right_frame;
	  last=actual;
	}
  }

  // kill producer
  stop_producer.store(1);

}

void view() {

  while ( !stop_viewer.load() ) {
	  if(!left_image.empty()) {
		cv::imshow("left frame", left_image);
		cv::imshow("right frame", right_image);
		cv::waitKey(10);
	  }
  }

}

int main(int argc, char *argv[])
{

  // register signal SIGINT and signal handler
  signal(SIGINT, signalHandler);


  // check if camera is connected
  if ( !bb_camera.isOpened() ) {
    std::cerr << "[" <<  argv[0] << "]: Error: BB camera is not connected" << std::endl;
    return -1;
  }


  std::cout << "framerate: "<< bb_camera.getFrameRate() << std::endl;

  int ret=0;
  ret = mkdir ("left_images", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
  if (ret != 0 && errno != EEXIST)
  {
	  printf("Error creating directory, errno:%d!\n", errno);
	  exit(1);
  }
  ret = mkdir ("right_images", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
  if (ret != 0 && errno != EEXIST)
  {
	  printf("Error creating directory, errno:%d!\n", errno);
	  exit(1);
  }

  Queue<data> q;

  // Start the producer thread.
  std::thread producer(std::bind(produce, std::ref(q)));

  // Start the consumer threads.
  std::thread consumer(std::bind(&consume, std::ref(q)));

  // Start viewer
  std::thread viewer(view);

  while( !stop_main.load() )
  {
      sleep(10); // Signals will interrupt this function.
  }

  producer.join();
  consumer.join();
  viewer.join();

  std::cout << "End" << std::endl;
}
