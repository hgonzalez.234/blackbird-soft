#include <stdio.h>
#include <string.h>
#include <csignal>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <iostream>
#include <fstream>

#include <opencv2/opencv.hpp>
#include "BlackbirdCamera.hpp"
#include "./utils/Logger.hpp"
#include "./utils/timer.h"

#define BOARD_WIDTH 9
#define BOARD_HEIGHT 6

std::vector< std::vector< cv::Point3f > > left_object_points;
std::vector< std::vector< cv::Point3f > > right_object_points;
std::vector< std::vector< cv::Point3f > > both_object_points;
std::vector< cv::Point2f > corners_left;
std::vector< cv::Point2f > corners_right;
std::vector< std::vector< cv::Point2f > > left_img_points;
std::vector< std::vector< cv::Point2f > > right_img_points;
std::vector< std::vector< cv::Point2f > > imagePoints1, imagePoints2;
std::vector< std::vector< cv::Point2f > > stereo_l_img_points, stereo_r_img_points;

void signalHandler( int signum ) {
   std::cout << "Interrupt signal (" << signum << ") received." << std::endl;

   // cleanup and close up stuff here
   // terminate program

   std::exit(signum);
}

double computeReprojectionErrors(const std::vector< std::vector< cv::Point3f > >& objectPoints,
                                 const std::vector< std::vector< cv::Point2f > >& imagePoints,
                                 const std::vector< cv::Mat >& rvecs, const std::vector< cv::Mat >& tvecs,
                                 const cv::Mat& cameraMatrix , const cv::Mat& distCoeffs) {
  std::vector< cv::Point2f > imagePoints2;
  int i, totalPoints = 0;
  double totalErr = 0, err;
  std::vector< float > perViewErrors;
  perViewErrors.resize(objectPoints.size());

  for (i = 0; i < (int)objectPoints.size(); ++i) {
    projectPoints(cv::Mat(objectPoints[i]), rvecs[i], tvecs[i], cameraMatrix,
                  distCoeffs, imagePoints2);
    err = norm(cv::Mat(imagePoints[i]), cv::Mat(imagePoints2), CV_L2);
    int n = (int)objectPoints[i].size();
    perViewErrors[i] = (float) std::sqrt(err*err/n);
    totalErr += err*err;
    totalPoints += n;
  }
  return std::sqrt(totalErr/totalPoints);
}

void stereo_calibration(const std::vector<std::pair<cv::Mat, cv::Mat> >& imgs, float square_size) {
  cv::Size board_size = cv::Size(BOARD_WIDTH, BOARD_HEIGHT);

  for (auto& img_pair: imgs) {
	bool found_left = false;
	bool found_right = false;
	cv::Mat gray_left, gray_right;

	cv::cvtColor(img_pair.first, gray_left, CV_BGR2GRAY);
    cv::cvtColor(img_pair.second, gray_right, CV_BGR2GRAY);
	//Dig a bit on those flags.
	found_left = cv::findChessboardCorners(img_pair.first, board_size, corners_left,
									       CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);
	found_right = cv::findChessboardCorners(img_pair.second, board_size, corners_right,
											CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);
	std::vector< cv::Point3f > obj;
    for (int i = 0; i < BOARD_HEIGHT; i++)
      for (int j = 0; j < BOARD_WIDTH; j++)
        obj.push_back(cv::Point3f((float)j * square_size, (float)i * square_size, 0));

	if (found_left)
    {
      //Dig on this too.
	  cv::cornerSubPix(gray_left, corners_left, cv::Size(5, 5), cv::Size(-1, -1),
                      cv::TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
	  cv::drawChessboardCorners(gray_left, board_size, corners_left, found_left);
      left_img_points.push_back(corners_left);
      left_object_points.push_back(obj);
	}

	if (found_right)
    {
      //Dig on this too.
	  cv::cornerSubPix(gray_right, corners_right, cv::Size(5, 5), cv::Size(-1, -1),
                   cv::TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
	  cv::drawChessboardCorners(gray_right, board_size, corners_right, found_right);
      right_img_points.push_back(corners_right);
      right_object_points.push_back(obj);
	}

	if (found_left && found_right) {
	  std::cout << "Found corners!" << std::endl;
      imagePoints1.push_back(corners_left);
      imagePoints2.push_back(corners_right);
      both_object_points.push_back(obj);
    }

  }

  cv::Mat K_left, K_right;
  cv::Mat D_left, D_right;
  std::vector< cv::Mat > l_rvecs, l_tvecs;
  std::vector< cv::Mat > r_rvecs, r_tvecs;
  int flag = 0;
  //flag |= cv::CV_CALIB_FIX_K4;
  //flag |= cv::CV_CALIB_FIX_K5;

  cv::calibrateCamera(left_object_points, left_img_points, imgs[0].first.size(), K_left, D_left, l_rvecs, l_tvecs, flag);
  std::cout << "Left Camera Calibration error: " <<
	  computeReprojectionErrors(left_object_points, left_img_points, l_rvecs, l_tvecs, K_left, D_left) << std::endl;
  cv::calibrateCamera(right_object_points, right_img_points, imgs[0].second.size(), K_right, D_right, r_rvecs, r_tvecs, flag);
  std::cout << "Right Camera Calibration error: " <<
	  computeReprojectionErrors(right_object_points, right_img_points, r_rvecs, r_tvecs, K_right, D_right) << std::endl;

  for (int i = 0; i < imagePoints1.size(); i++) {
	std::vector< cv::Point2f > v1, v2;
    for (int j = 0; j < imagePoints1[i].size(); j++) {
      v1.push_back(cv::Point2f((double)imagePoints1[i][j].x, (double)imagePoints1[i][j].y));
      v2.push_back(cv::Point2f((double)imagePoints2[i][j].x, (double)imagePoints2[i][j].y));
    }
    stereo_l_img_points.push_back(v1);
    stereo_r_img_points.push_back(v2);
  }

  cv::Mat R, F, E;
  cv::Vec3d T;

  cv::stereoCalibrate(both_object_points, stereo_l_img_points, stereo_r_img_points, K_left, D_left, K_right, D_right,
					  imgs[0].first.size(), R, T, E, F);

  std::ofstream calib_left("calib_left.txt");
  std::ofstream calib_right("calib_right.txt");
  calib_left << imgs[0].first.cols << " " << imgs[0].first.rows << std::endl << std::endl;
  calib_left << K_left.at<float>(0,0) << " " << K_left.at<float>(0,1) << " " << K_left.at<float>(0,2) << std::endl;
  calib_left << K_left.at<float>(1,0) << " " << K_left.at<float>(1,1) << " " << K_left.at<float>(1,2) << std::endl;
  calib_left << K_left.at<float>(2,0) << " " << K_left.at<float>(2,1) << " " << K_left.at<float>(2,2) << std::endl << std::endl;
  calib_left << D_left.at<float>(0,0) << " " << D_left.at<float>(0,1) << " " << D_left.at<float>(0,2) << " ";
  calib_left << D_left.at<float>(0,3) << " " << D_left.at<float>(0,4) << std::endl << std::endl;
  calib_left << "1 0 0" << std::endl << "0 1 0" << std::endl << "0 0 1" << std::endl << std::endl;
  calib_left << "0 0 0" << std::endl;

  calib_right << imgs[0].second.cols << " " << imgs[0].second.rows << std::endl << std::endl;
  calib_right << K_right.at<float>(0,0) << " " << K_right.at<float>(0,1) << " " << K_right.at<float>(0,2) << std::endl;
  calib_right << K_right.at<float>(1,0) << " " << K_right.at<float>(1,1) << " " << K_right.at<float>(1,2) << std::endl;
  calib_right << K_right.at<float>(2,0) << " " << K_right.at<float>(2,1) << " " << K_right.at<float>(2,2) << std::endl << std::endl;
  calib_right << D_right.at<float>(0,0) << " " << D_right.at<float>(0,1) << " " << D_right.at<float>(0,2) << " ";
  calib_right << D_right.at<float>(0,3) << " " << D_right.at<float>(0,4) << std::endl << std::endl;
  calib_right << R.at<float>(0,0) << " " << R.at<float>(0,1) << " " << R.at<float>(0,2) << std::endl;
  calib_right << R.at<float>(1,0) << " " << R.at<float>(1,1) << " " << R.at<float>(1,2) << std::endl;
  calib_right << R.at<float>(2,0) << " " << R.at<float>(2,1) << " " << R.at<float>(2,2) << std::endl << std::endl;
  calib_right << T[0] << " " << T[1] << " " << T[2] << std::endl;
}


int main(int argc, char *argv[]) {

  // register signal SIGINT and signal handler
  signal(SIGINT, signalHandler);

  // bb parameters
  double frame_rate = 15.0; // 15-60 hz

  // create camera bb object
  BBCamera bb_camera(frame_rate);

  // check if camera is connected
  if ( !bb_camera.isOpened() ) {
    std::cerr << "[" <<  argv[0] << "]: Error: Blackbird camera is not connected" << std::endl;
    return -1;
  }

  std::cout << "framerate: "<< bb_camera.getFrameRate() << std::endl;

  cv::Mat left_frame, right_frame;
  std::vector< std::pair< cv::Mat, cv::Mat >> calib_imgs;
  int ret=0;
  ret = mkdir ("calib_images", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
  if (ret != 0 && errno != EEXIST)
  {
	  printf("Error creating directory, errno:%d!\n", errno);
	  exit(1);
  }

  //Streams the Camera using the OpenCV Video Capture Object
  // get timestamp
  auto previous_timestamp = Timer::now();
  auto last_saved = previous_timestamp;
  int keycode = 0;
  bool finish = false;
  int imgcounter = 0;
  std::cout << "Press 'c' to capture the images" << std::endl;
  while ( !finish ) {
	  bb_camera.getImages(left_frame, right_frame);
	  cv::imshow("left frame", left_frame);
      cv::imshow("right frame", right_frame);
	  keycode = cv::waitKey(10);
	  auto current_timestamp = Timer::now(); //time in seconds with precision of milliseconds

	  previous_timestamp = current_timestamp;
	  if (current_timestamp > last_saved + 0.1 && keycode == 99) {
		  cv::imwrite("calib_images/left_"+std::to_string(current_timestamp)+".png",left_frame);
		  cv::imwrite("calib_images/right_"+std::to_string(current_timestamp)+".png",right_frame);
		  last_saved += 0.1;
		  calib_imgs.push_back(std::make_pair(left_frame, right_frame));
		  imgcounter++;
	  }
	  if (keycode == 102)
		  finish = true;
  }

  float pattern_size = 0;
  std::cout << "Finished capturing images: " << imgcounter << " images captured." << std::endl;
  std::cout << "Starting Intrinsic Calibration... " << std::endl;
  std::cout << "Please enter the size of the pattern square (in meters): " << std::endl;
  std::cin >> pattern_size;
  std::cout << "Pattern size: " << pattern_size << std::endl;
  if (pattern_size <= 0.0) {
	  std::cout << "Pattern size should be positive and greater than 0." << std::endl;
      return 1;
  }

  //TODO: Implemented only for 9x6 patterns, add different ones.
  stereo_calibration(calib_imgs, pattern_size);
  std::cout << "Finito!" << std::endl;
  return 0;
}
