/**
 * This file is part of S-PTAM.
 *
 * Copyright (C) 2013-2017 Taihú Pire
 * Copyright (C) 2014-2017 Thomas Fischer
 * Copyright (C) 2016-2017 Gastón Castro
 * Copyright (C) 2017 Matias Nitsche
 * For more information see <https://github.com/lrse/sptam>
 *
 * S-PTAM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * S-PTAM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with S-PTAM. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:  Taihú Pire
 *           Thomas Fischer
 *           Gastón Castro
 *           Matías Nitsche
 *
 * Laboratory of Robotics and Embedded Systems
 * Department of Computer Science
 * Faculty of Exact and Natural Sciences
 * University of Buenos Aires
 */
#include <iomanip>
#include <ios>
#include <cassert>
#include "timer.h"
#include <sys/time.h> // gettimeofday


Timer::Timer(void) :
  elapsed_seconds(0), started(false)
{

}

void Timer::start(void)
{
  assert(!started);
  gettimeofday(&t, NULL);
  started = true;
}

void Timer::stop(void)
{
  assert(started);
  struct timeval t_stop;
  gettimeofday(&t_stop, NULL);
  elapsed_seconds += (1000 * (t_stop.tv_sec - t.tv_sec) + (t_stop.tv_usec - t.tv_usec) / 1000) / 1000.0; //elapsed time in seconds with precision of milliseconds
  started = false;
}

double Timer::elapsed(void) const
{
  return elapsed_seconds;
}

double Timer::now()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return ((1000 * (long long)tv.tv_sec) + (tv.tv_usec / 1000)) / 1000.0; //time in seconds with precision of milliseconds
}


std::ostream& operator<< (std::ostream& stream, const Timer& t)
{
  stream << std::setprecision(16) << std::fixed << t.elapsed();
  return stream;
}
