/**
 * This file is part of S-PTAM.
 *
 * Copyright (C) 2013-2017 Taihú Pire
 * Copyright (C) 2014-2017 Thomas Fischer
 * Copyright (C) 2016-2017 Gastón Castro
 * Copyright (C) 2017 Matias Nitsche
 * For more information see <https://github.com/lrse/sptam>
 *
 * S-PTAM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * S-PTAM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with S-PTAM. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:  Taihú Pire
 *           Thomas Fischer
 *           Gastón Castro
 *           Matías Nitsche
 *
 * Laboratory of Robotics and Embedded Systems
 * Department of Computer Science
 * Faculty of Exact and Natural Sciences
 * University of Buenos Aires
 */

#pragma once

#include <list>
#include <eigen3/Eigen/Geometry>
// lexical cast apparently automatically sets the precision for the float string.
#include <boost/lexical_cast.hpp>

#include "Logger.hpp"
#include "timer.h"

inline std::string printFullPrecision( double x )
{
  return std::to_string( x );
//  return boost::lexical_cast<std::string>( x ); // for TimerChrono we should use this
}

namespace std {
  inline std::ostream& lazy_endl( std::ostream& os ){
      os.put('\n');

  #ifdef ENABLE_LOG_FLUSH
      os.flush();
  #endif

      return os;
  }
}

#define STREAM_TO_LOG( msg ) { std::stringstream message; message << msg << std::endl; Logger::Write( message.str() ); }

template<typename T>
inline void WriteToLog(const std::string& tag, const T& n)
{
  std::stringstream message;
  message << printFullPrecision( Timer::now() ) << tag << n << std::lazy_endl; // for TimerChrono we should change this

  Logger::Write( message.str() );
}

template<typename T, typename U>
inline void writeToLog(const std::string& tag, const T& a, const U& b)
{
  std::stringstream message;
  message << tag << " " << a << " " << b << std::lazy_endl;
  Logger::Write( message.str() );
}

template<typename T, typename U, typename V>
inline void writeToLog(const std::string& tag, const T& a, const U& b, const V& c)
{
  std::stringstream message;
  message << tag << " " << a << " " << b << " " << c << std::lazy_endl;
  Logger::Write( message.str() );
}

template<typename T>
inline void WriteToLog(const std::string tag, const std::list<T>& list)
{
  std::stringstream message;

  message << printFullPrecision( Timer::now() ) << tag; // for TimerChrono we should change this

  for ( const auto& elem : list )
    message << " " << elem;

  message << std::lazy_endl;

  Logger::Write( message.str() );
}
