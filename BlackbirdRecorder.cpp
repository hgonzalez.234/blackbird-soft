#include <stdio.h>
#include <string.h>
#include <csignal>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>

#include <opencv2/opencv.hpp>
#include "BlackbirdCamera.hpp"
#include "./utils/Logger.hpp"
#include "./utils/timer.h"


void signalHandler( int signum ) {
   std::cout << "Interrupt signal (" << signum << ") received." << std::endl;

   // cleanup and close up stuff here
   // terminate program

   std::exit(signum);
}

int main(int argc, char *argv[]) {

  // register signal SIGINT and signal handler
  signal(SIGINT, signalHandler);

  // bb parameters
  double frame_rate = 15.0; // 15-60 hz

  // create camera bb object
  BBCamera bb_camera(frame_rate);

  // check if camera is connected
  if ( !bb_camera.isOpened() ) {
    std::cerr << "[" <<  argv[0] << "]: Error: Blackbird camera is not connected" << std::endl;
    return -1;
  }

  std::cout << "framerate: "<< bb_camera.getFrameRate() << std::endl;

  cv::Mat left_frame, right_frame;
  int ret=0;
  ret = mkdir ("left_images", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
  if (ret != 0 && errno != EEXIST)
  {
	  printf("Error creating directory, errno:%d!\n", errno);
	  exit(1);
  }
  ret = mkdir ("right_images", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );
  if (ret != 0 && errno != EEXIST)
  {
	  printf("Error creating directory, errno:%d!\n", errno);
	  exit(1);
  }

  //Streams the Camera using the OpenCV Video Capture Object
  // get timestamp
  auto previous_timestamp = Timer::now();
  auto last_saved = previous_timestamp;
  while ( true ) {
    bb_camera.getImages(left_frame, right_frame);

    // get timestamp
    auto current_timestamp = Timer::now(); //time in seconds with precision of milliseconds
    if (current_timestamp - previous_timestamp >= 0.1) {
      printf("frame rate:  %f s\n",current_timestamp - previous_timestamp);
      printf("Camera:  %f s\n",current_timestamp);

     }
     previous_timestamp = current_timestamp;
     if (current_timestamp > last_saved + 0.1) {
       cv::imwrite("left_images/left_"+std::to_string(current_timestamp)+".png",left_frame);
       cv::imwrite("right_images/right_"+std::to_string(current_timestamp)+".png",right_frame);
       last_saved += 0.1;
     }
  }

  return 0;
}
